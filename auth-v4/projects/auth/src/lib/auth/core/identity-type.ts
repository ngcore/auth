/**
 * Method used to verify identity.
 */
export enum IdentityType {
  unknown = 0,
  email = 1,
  phone = 2,
  // sms = 3,  // same as phone...
  // ...
  // social login, etc. ???
  // ...
}
