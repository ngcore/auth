import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { DateTimeUtil, UniqueIdUtil } from '@ngcore/core';
import { BaseModel } from '@ngcore/base';


/**
 * Option for the device/computer/browser.
 * There can be at most one instance of this object stored on a given silo.
 */
export class LoginOption extends BaseModel {

  private static GLOBAL_LOGIN_OPTION_ID: string = "000000000101";

  // Note; nickname is the primary key.
  static getDocId(): string {
    return LoginOption.GLOBAL_LOGIN_OPTION_ID;
  }
  getDocId(): string {
    return LoginOption.GLOBAL_LOGIN_OPTION_ID;
  }
  setDocId(_id: string) {
    // ignore.
  }

  // If set, this nickname will be automatically selected without prompting the user.
  // Even if it's set, if the nickname does not exist (because it waa deleted or renamed, etc.), we do not use this value.
  // Note that we store the nickname string, not UserNickname or Account object.
  // defaultAccountNickname: string = null;

  // tbd:
  // timestamps ???
  // ....

  constructor(public defaultAccountNickname: (string | null) = null) {
    super(LoginOption.GLOBAL_LOGIN_OPTION_ID, LoginOption.GLOBAL_LOGIN_OPTION_ID);
  }


  toString(): string {
    return super.toString()
      + 'defaultAccountNickname = ' + this.defaultAccountNickname;
  }


  clone(): LoginOption {
    let cloned = Object.assign(new LoginOption(), this) as LoginOption;
    return cloned;
  }
  static clone(obj: any): LoginOption {
    let cloned = Object.assign(new LoginOption(), obj) as LoginOption;
    return cloned;
  }

  copy(): LoginOption {
    let obj = this.clone();
    // obj.id = RandomIdUtil.id();
    obj.id = UniqueIdUtil.id();
    // userId ????
    obj.resetCreatedTime();
    obj.defaultAccountNickname = null;   // ???
    return obj;
  }
}
