import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { DateTimeUtil, RandomIdUtil } from '@ngcore/core';
import { IdentityType } from '../core/identity-type';


/**
 * Represents "auth identity".
 */
export class AuthIdentity {

  //   static getId(authId: string, authIdType: IdentityType): string {
  //     return authId + ':' + authIdType;
  //   }
  getId(): string {
    return this.authId + ':' + this.authIdType;
  }
  setId(_id: string) {
    let idx = -1;
    if (_id) {
      idx = _id.indexOf(':');
    }
    if (idx == -1) {
      this.authId = _id;  // ???
      this.authIdType = IdentityType.unknown;   // ???
    } else {
      this.authId = _id.substring(0, idx);
      this.authIdType = (_id.length > idx + 1) ? parseInt(_id.substring(idx + 1)) as IdentityType : IdentityType.unknown;  // ???
    }
  }


  // tbd:
  // Move to a separate class, AuthCredential ???

  private _token: (string | null) = null;
  private _tokenExpirationTime: number = 0;
  private _tokenValidatedTime: number = 0;
  // private _passwd: string = null;
  // private _passwdCreatedTime: number = 0;

  getToken(): (string | null) {
    if (this._tokenExpirationTime == 0 || this._tokenExpirationTime <= DateTimeUtil.getUnixEpochMillis()) {
      return this._token;
    } else {
      return null;
    }
  }
  setToken(_token: string, _tokenExpirationTime: number = 0) {
    this._token = _token;
    this._tokenExpirationTime = _tokenExpirationTime;
  }
  get isTokenValidated(): boolean {
    return (this._token != null) && (this._tokenValidatedTime > 0);
  }

  // get hasPasswd(): boolean {
  //   return (this._passwd != null);
  // }
  // get passwd(): string {
  //   return this._passwd;
  // }
  // set passwd(_passwd: string) {
  //   this._passwd = _passwd;
  //   this._passwdCreatedTime = (this._passwd) ? DateTimeUtil.getUnixEpochMillis() : 0;
  // }
  // get passwdCreatedTime(): number {
  //   return this._passwdCreatedTime;
  // }

  // etc..
  // ...


  // If true, this identity (authId) will be used for notifications.
  isEnabledForNotifications: boolean = false;


  // authId: email address, phone number, etc...
  // constructor(public authId: (string | null) = null, public authIdType: IdentityType = IdentityType.unknown) {
  authId: string;
  constructor(authId: (string | null) = null, public authIdType: IdentityType = IdentityType.unknown) {
    if (authId) {  // "0" is not a valid id. ????
      this.authId = authId;
    } else {
      this.authId = RandomIdUtil.id();
    }
  }


  toString(): string {
    return 'authId:' + this.authId
      + ';authIdType:' + this.authIdType
      + ';isEnabledForNotifications:' + this.isEnabledForNotifications
      + ';_token:' + this._token
      + ';_tokenExpirationTime:' + this._tokenExpirationTime
      + ';_tokenValidatedTime:' + this._tokenValidatedTime
      + ';getToken():' + this.getToken()
  }

  clone(): AuthIdentity {
    let cloned = Object.assign(new AuthIdentity(), this) as AuthIdentity;
    return cloned;
  }
  static clone(obj: any): AuthIdentity {
    let cloned = Object.assign(new AuthIdentity(), obj) as AuthIdentity;
    return cloned;
  }

  copy(): AuthIdentity {
    let obj = this.clone();
    // obj.bitId = 0;  // ???
    return obj;
  }

}
