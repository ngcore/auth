import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { ColorTag, ColorTagUtil, DateTimeUtil, UniqueIdUtil } from '@ngcore/core';
import { BaseModel } from '@ngcore/base';
import { IdentityType } from '../core/identity-type';
import { AuthIdentity } from './auth-identity'


/**
 * Represents User identity.
 */
// Vs. AppUser ????
// *** Not a storage class. ????
// This is more of a ViewModel than a Model.
export class UserAccount extends BaseModel {

  // Note:
  // nickname cannot be part of docId since it can change.
  // docId should be immutable once an object is saved in DB....
  // --> Just use BaseModel.docId for now...

  // static getDocId(userId: string, nickname: string): string {
  //   return userId + ':' + nickname;
  // }
  // getDocId(): string {
  //   return this.userId + ':' + this.nickname;
  // }
  // setDocId(_id: string) {
  //   let idx = -1;
  //   if (_id) {
  //     idx = _id.indexOf(':');
  //   }
  //   if (idx == -1) {
  //     this.userId = null;  // ???
  //     this.nickname = _id;
  //   } else {
  //     this.userId = _id.substring(0, idx);
  //     this.nickname = (_id.length > idx + 1) ? _id.substring(idx + 1) : "";  // ???
  //   }
  // }


  // tbd:
  // account name ?

  // TBD: Need to support different avatars/profile photos for different accounts...
  // colorTag: ColorTag;   // Use egg-shape/circular images ????
  // ....


  // TBD:
  // Use array????
  // ???

  //   public primaryAuthId: string = null;
  //   // getPrimaryAuthId(): string { return this.primaryAuthId; }
  //   setPrimaryAuthId(_primaryAuthId: string) { this.primaryAuthId = _primaryAuthId; this.isDirty = true; }

  //   public primaryAuthIdType: IdentityType = IdentityType.email;   // ???
  //   // getPrimaryAuthIdType(): IdentityType { return this.primaryAuthIdType; }
  //   setPrimaryAuthIdType(_primaryAuthIdType: IdentityType) { this.primaryAuthIdType = _primaryAuthIdType; this.isDirty = true; }

  //   public secondaryAuthId: string = null;
  //   // getSecondaryAuthId(): string { return this.secondaryAuthId; }
  //   setSecondaryAuthId(_secondaryAuthId: string) { this.secondaryAuthId = _secondaryAuthId; this.isDirty = true; }

  //   public secondaryAuthIdType: IdentityType = IdentityType.phone;   // ???
  //   // getSecondaryAuthIdType(): IdentityType { return this.secondaryAuthIdType; }
  //   setSecondaryAuthIdType(_secondaryAuthIdType: IdentityType) { this.secondaryAuthIdType = _secondaryAuthIdType; this.isDirty = true; }

  //   public tertiaryAuthId: string = null;
  //   // getTertiaryAuthId(): string { return this.tertiaryAuthId; }
  //   setTertiaryAuthId(_tertiaryAuthId: string) { this.tertiaryAuthId = _tertiaryAuthId; this.isDirty = true; }

  //   public tertiaryAuthIdType: IdentityType = IdentityType.email;   // ???
  //   // getTertiaryAuthIdType(): IdentityType { return this.tertiaryAuthIdType; }
  //   setTertiaryAuthIdType(_tertiaryAuthIdType: IdentityType) { this.tertiaryAuthIdType = _tertiaryAuthIdType; this.isDirty = true; }


  // TBD:
  // "move" operation for 3rd to 2nd, 2nd to 1st ?

  public primaryAuthIdentity: (AuthIdentity | null) = null;
  // getPrimaryAuthIdentity(): AuthIdentity { return this.primaryAuthIdentity; }
  setPrimaryAuthIdentity(_primaryAuthIdentity: (AuthIdentity | null)) { this.primaryAuthIdentity = _primaryAuthIdentity; this.isDirty = true; }
  resetPrimaryAuthIdentity() {
    this.setPrimaryAuthIdentity(null);
  }
  get hasPrimaryAuthIdentity() {
    return (this.primaryAuthIdentity != null);
  }

  public secondaryAuthIdentity: (AuthIdentity | null) = null;
  // getSecondaryAuthIdentity(): AuthIdentity { return this.secondaryAuthIdentity; }
  setSecondaryAuthIdentity(_secondaryAuthIdentity: (AuthIdentity | null)) { this.secondaryAuthIdentity = _secondaryAuthIdentity; this.isDirty = true; }
  resetSecondaryAuthIdentity() {
    this.setSecondaryAuthIdentity(null);
  }
  get hasSecondaryAuthIdentity() {
    return (this.secondaryAuthIdentity != null);
  }

  public tertiaryAuthIdentity: (AuthIdentity | null) = null;
  // getTertiaryAuthIdentity(): AuthIdentity { return this.tertiaryAuthIdentity; }
  setTertiaryAuthIdentity(_tertiaryAuthIdentity: (AuthIdentity | null)) { this.tertiaryAuthIdentity = _tertiaryAuthIdentity; this.isDirty = true; }
  resetTertiaryAuthIdentity() {
    this.setTertiaryAuthIdentity(null);
  }
  get hasTertiaryAuthIdentity() {
    return (this.tertiaryAuthIdentity != null);
  }

  // TBD:
  // Social identities.
  // Note that there can be at most one identity per social login provider.
  //    That is, one account cannot be associated with more than one Facebook account.
  // ....



  // Password is associated with an account not with an identity.
  // Certain AuthIdentities may not require custom passwords (e.g., social identities).
  // ...

  // TBD:
  //     Last Identity/token used to create/reset password????

  private _passwd: (string | null) = null;
  private _passwdCreatedTime: number = 0;

  get hasPasswd(): boolean {
    return (this._passwd != null);
  }
  get passwd(): (string | null) {
    return this._passwd;
  }
  set passwd(_passwd: (string | null)) {
    this._passwd = _passwd;
    this._passwdCreatedTime = (this._passwd != null) ? DateTimeUtil.getUnixEpochMillis() : 0;
  }
  get passwdCreatedTime(): number {
    return this._passwdCreatedTime;
  }


  // For the following, if at least one of the authIdentity satisfies the condtion, the overall answer is true...

  // TBD:
  // ???
  get isVerified(): boolean {
    // tbd: Check authIdentity.isTokenValidated for each children...
    return true;
  }
  // How to do this?
  private _isAuthenticated: boolean = false;
  get isAuthenticated(): boolean {
    // ???? 
    return this._isAuthenticated;
  }
  setAuthenticated(_isAuthenticated: boolean) {
    // ???? 
    this._isAuthenticated = _isAuthenticated;
    this.isDirty = true;
  }
  // ....


  public isLocal: boolean = false;
  // getIsLocal(): boolean { return this.isLocal; }
  setIsLocal(_isLocal: boolean) { this.isLocal = _isLocal; this.isDirty = true; }

  public isHidden: boolean = false;
  // getIsHidden(): boolean { return this.isHidden; }
  setIsHidden(_isHidden: boolean) { this.isHidden = _isHidden; this.isDirty = true; }


  // temporary
  // To distinguish an empty object vs "real" object....
  // TBD: is there a better way?
  get isInitialized(): boolean {
    return (this.nickname != null);
  }
  // temporary


  // If true, the account will be assumed to be logged on once selected by the user.
  // It can be set to true, only if any identity of the account has been validated
  //    (at least at the time when this switch is turned on). 
  isKeepLoggedOnSet: boolean = false;


  // Here vs in AppUser/UserProfile ???
  //   public nickname: string = null;
  //   // getNickname(): string { return this.nickname; }
  //   setNickname(_nickname: string) { this.nickname = _nickname; this.isDirty = true; }

  // getNickname(): string { return this.nickname; }
  setNickname(_nickname: string) { this.nickname = _nickname; this.isDirty = true; }

  // get userId(): string { return this._userId; }
  // set userId(_userId: string) { this._userId = _userId; this.isDirty = true; }

  // Note: We use RandomId instead of UniqueId for userId.
  //       This is necessary because userId can be used as Hash Key in DynamoDB.
  // TBD: Refactor ctor to take id (== "account id") as an arg????
  constructor(private _uid: (string | null) = null, public nickname: (string | null) = null) {
    super(null, _uid);
  }


  toString(): string {
    let str = super.toString()
      + '; userId = ' + this.userId
      + '; nickname = ' + this.nickname
      + '; isKeepLoggedOnSet = ' + this.isKeepLoggedOnSet;

      // authidentity???
      if(this.primaryAuthIdentity) {
        str += '; primaryAuthIdentity = {' + this.primaryAuthIdentity.toString() + '}'; 
      }
      if(this.secondaryAuthIdentity) {
        str += '; secondaryAuthIdentity = {' + this.secondaryAuthIdentity.toString() + '}'; 
      }
      if(this.tertiaryAuthIdentity) {
        str += '; tertiaryAuthIdentity = {' + this.tertiaryAuthIdentity.toString() + '}'; 
      }

      return str;
  }



  clone(): UserAccount {
    let cloned = Object.assign(new UserAccount(), this) as UserAccount;
    // ???
    // Sometimes, the object loses type information, and .clone() method throws exception...
    // if (this.primaryAuthIdentity) cloned.primaryAuthIdentity = this.primaryAuthIdentity.clone();
    // if (this.secondaryAuthIdentity) cloned.secondaryAuthIdentity = this.secondaryAuthIdentity.clone();
    // if (this.tertiaryAuthIdentity) cloned.tertiaryAuthIdentity = this.tertiaryAuthIdentity.clone();
    // Instead, just do this ???
    if (this.primaryAuthIdentity) cloned.primaryAuthIdentity = Object.assign(new AuthIdentity(), this.primaryAuthIdentity) as AuthIdentity;
    if (this.secondaryAuthIdentity) cloned.secondaryAuthIdentity = Object.assign(new AuthIdentity(), this.secondaryAuthIdentity) as AuthIdentity;
    if (this.tertiaryAuthIdentity) cloned.tertiaryAuthIdentity = Object.assign(new AuthIdentity(), this.tertiaryAuthIdentity) as AuthIdentity;
    return cloned;
  }
  static clone(obj: any): UserAccount {
    let cloned = Object.assign(new UserAccount(), obj) as UserAccount;
    if (obj.primaryAuthIdentity) cloned.primaryAuthIdentity = Object.assign(new AuthIdentity(), obj.primaryAuthIdentity) as AuthIdentity;
    if (obj.secondaryAuthIdentity) cloned.secondaryAuthIdentity = Object.assign(new AuthIdentity(), obj.secondaryAuthIdentity) as AuthIdentity;
    if (obj.tertiaryAuthIdentity) cloned.tertiaryAuthIdentity = Object.assign(new AuthIdentity(), obj.tertiaryAuthIdentity) as AuthIdentity;
    return cloned;
  }

  copy(): UserAccount {
    let obj = this.clone();
    // obj.id = RandomIdUtil.id();
    obj.id = UniqueIdUtil.id();
    // userId ????
    obj.resetCreatedTime();
    return obj;
  }
}
