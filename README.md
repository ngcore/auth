# @ngcore/auth
> NG Core angular/typescript auth library


Auth classes library for Angular.
(Current version requires Angular v7.2+)


## API Docs

* http://ngcore.gitlab.io/auth/



